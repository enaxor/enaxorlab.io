---
title: "TechWave: an IT blog"

description: "A Hugo site built with GitLab Pages"
cascade:
  featured_image: '/images/gohugo-default-sample-hero-image.jpg'
---

# Unleashing the Power of Big Data: A Passionate Woman's Perspective on Sysadmin

Welcome to my blog, where I dive into the fascinating world of big data and system administration on ArchLinux.

Happy reading !

~ enaxor