---
date: 2017-04-09T10:58:08-04:00
description: "Just in case I forgot again about regex"
featured_image: ""
tags: ["Regex", "Shell"]
title: "The Most Commonly Used Regex in Shell Scripting"
---

# The Most Commonly Used Regex in Shell Scripting

Regular expressions (regex) are powerful tools used for pattern matching and text manipulation. In the context of shell scripting, regex can be incredibly
 useful for tasks such as input validation, string extraction, and file processing. In this blog post, we will explore some of the most commonly used regex
 patterns in shell scripting and demonstrate their practical applications.

## 1. Matching a Numeric Value

One of the fundamental tasks in shell scripting is validating numeric input. To check if a variable contains only numeric characters, you can use the
 following regex pattern:

```bash
^[0-9]+$
```

This pattern ensures that the entire string consists of one or more digits (0-9). You can incorporate this pattern in your script to ensure that user input
 meets the expected format.

## 2. Validating Email Addresses

Validating email addresses is a common requirement in many scripts. Although email validation can be complex, a basic regex pattern can help verify whether
 an email address has a valid structure:

```bash
^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$
```

This pattern checks if an email address contains alphanumeric characters, dot, underscore, percentage, plus, or hyphen before the "@" symbol. After the "@",
 it verifies the presence of alphanumeric characters and dots before the domain extension (e.g., .com, .org).

## 3. Extracting File Extensions

In shell scripting, extracting file extensions can be useful when performing operations on specific file types. To extract the extension from a file name,
 you can use the following regex pattern:

```bash
\.[^.]+$

```

This pattern matches the last occurrence of "." followed by one or more characters that are not a dot. It effectively captures the file extension at the end
 of a filename, allowing you to process files based on their types.

## 4. Searching for Patterns in Text

Shell scripting often involves searching for specific patterns within text files or command outputs. The grep command is commonly used for this purpose,
 and it supports regex patterns. Here's an example of using grep with a regex pattern to find lines containing a specific word:

```bash
grep -E 'pattern' file.txt
```

The -E option enables extended regular expressions, allowing for more advanced pattern matching.

## 5. Matching IP Addresses

Validating IP addresses is another common task in shell scripting. To ensure that an input string represents a valid IP address, you can utilize the
 following regex pattern:

```bash
^([0-9]{1,3}\.){3}[0-9]{1,3}$
```

This pattern matches four sets of one to three digits separated by periods. Each set must be between 0 and 255. By using this regex pattern, you can confirm
 if an IP address has the correct format before further processing.

## Conclusion

Regular expressions are invaluable tools in shell scripting for pattern matching and text manipulation tasks. In this blog post, we have covered some of the
 most commonly used regex patterns in shell scripting, including matching numeric values, validating email addresses, extracting file extensions, searching
 for patterns in text, and matching IP addresses. Understanding and utilizing these regex patterns will enhance your ability to write effective shell scripts
 and automate various tasks in your workflow.


