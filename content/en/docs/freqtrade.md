---
date: 2017-04-09T10:58:08-04:00
description: "Getting Started with freqtrade"
featured_image: "https://www.freqtrade.io/en/stable/assets/freqtrade_poweredby.svg"
tags: ["freqtrade", "trading"]
title: "My freqtrade template stategy"
---

# My freqtrade template stategy

![alt text](https://www.freqtrade.io/en/stable/assets/freqtrade_poweredby.svg)

Here's a step-by-step guide on how I use Freqtrade every day for my paper bots:

## Install Freqtrade:
Start by installing Freqtrade, a popular open-source cryptocurrency trading bot framework. You can install it using pip by running the following command in your terminal:

```shell
pip install freqtrade
```

## Create a New Strategy:
Next, create a new Python file for your strategy. You can name it anything you like, but make sure it has a .py extension. For example, let's call it my_strategy.py.

## Import the Required Modules:
In your strategy file, import the necessary modules. These typically include freqtrade.strategy and any additional modules required by the indicators you plan to use. For example:

```python
from freqtrade.strategy import IStrategy
import talib
```

## Define Your Strategy Class and Implement the Required Methods:
Inside your strategy class, implement the required methods such as populate_indicators() and populate_buy_trend(). These methods will define the indicators and the buy/sell logic for your strategy. Here's an example:

```python
    class MyStrategy(IStrategy):
        def populate_indicators(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
            # Use talib to calculate your indicators
            dataframe['sma'] = talib.SMA(dataframe['close'], timeperiod=20)
            dataframe['rsi'] = talib.RSI(dataframe['close'], timeperiod=14)
            dataframe['macd'], _, _ = talib.MACD(dataframe['close'])

            return dataframe

        def populate_buy_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
            # Define your buy logic based on the indicators
            dataframe.loc[
                (dataframe['close'] > dataframe['sma']) &
                (dataframe['rsi'] < 30) &
                (dataframe['macd'] > 0),
                'buy'] = 1

            return dataframe
```

In this example, we calculate three indicators: Simple Moving Average (SMA), Relative Strength Index (RSI), and Moving Average Convergence Divergence (MACD). We then define the buy logic based on these indicators.

## Customize Your Strategy:
Feel free to customize the strategy further by adding additional indicators or modifying the buy/sell logic to suit your trading preferences.

## Run Freqtrade:
Once you have defined your strategy, you can run Freqtrade using the command line interface (CLI). Make sure you have configured your Freqtrade settings, such as exchange API keys and trading pairs, before running the bot. To start Freqtrade with your strategy, run the following command:

```shell
freqtrade trade --strategy my_strategy
```

Replace my_strategy with the actual name of your strategy file. And that's it! 